# Description

GPT Migrate creates an exact copy of one hard drive with a GUID partition table to another hard drive of larger/different size.
But It fixes the new GPT's location, entries, checksums and the new protective MBR according to the new hard drive's size.

Arguments must be device files located in /dev.

The first argument should be the source device, the second the target device.

# Dependencies

- Perl 5
  - Archive::Zip
    - install 'libarchive-zip-perl' on Debian-based systems
    - or 'perl-archive-zip' on Arch-based systems
- dd (except '--no-dd' switch is used)

- a valid GUID partition table on /dev/\<source\>
- LBA size of 512 Bytes on both drives

# Install

```
$ wget https://gitlab.com/abakus_lab/gptmigrate/-/archive/v0.2/gptmigrate-v0.2.tar.gz
$ tar zxf gptmigrate-v0.2.tar.gz
$ cd gptmigrate-v0.2/
$ sudo bash install.sh
```

# Uninstall

```
$ sudo bash uninstall.sh
```

# Usage
```
$ man gptmigrate
```
```
$ sudo gptmigrate.pl [OPTIONS] /dev/<source> /dev/<target>
```
