#!/bin/bash

declare FILE
declare CHKSUM

f_uninstall() {
  if [[ -e ${FILE} ]]; then

    if [[ "$(sha1sum ${FILE} | cut -f1 -d' ')" != "${CHKSUM}" ]]; then

      declare ANS

      echo "${FILE} has been modified."
      echo -n "remove it anyway? [Y/n] "
      read ANS

      if [[ "${ANS}" =~ ^[nN].* ]]; then
	return
      fi

    fi

    rm ${FILE} && echo "${FILE} has been removed"

  else
    echo "${FILE} doesn't exist -> not removed."
  fi
}

FILE="/usr/local/sbin/gptmigrate.pl"
CHKSUM="502425e335ad577a8c4c3c01e39002d58e341a6d"
f_uninstall

FILE="/usr/local/man/man8/gptmigrate.8.gz"
CHKSUM="49fe185ec0aa9fc8a7dd2cb7e34306e09831cfda"
f_uninstall
