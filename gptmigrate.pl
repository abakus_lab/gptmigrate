#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long qw(GetOptions);
Getopt::Long::Configure qw(gnu_getopt);
use Archive::Zip;

##########################
#          READ          #
##########################

# define release version and a usage string
my $release_version = "v0.2";
my $usage = (
  "Usage:\n$0 [[--blocksize|--bs|-b <dd_blocksize>]|[--no-dd|--nodd|-n]] " .
  "[--verbose|-v] /dev/<source> /dev/<target>" .
  "\n$0 --version|-V"
);

# read command line options
my $blksize;
my $no_dd;
my $verbose;
my $print_version;
GetOptions(
  'blocksize|bs|b=s' => \$blksize,
  'no-dd|nodd|n' => \$no_dd,
  'verbose|v' => \$verbose,
  'version|V' => \$print_version
) or die "$usage\n";
if ($blksize and $no_dd) {
  die "$usage\n";
}

# print release version and exit
if ($print_version) {
  print("$release_version\n");
  exit(0);
}

# read device names from command line arguments
if (scalar @ARGV != 2) {
  print(STDERR "$usage\n");
  exit(1);
}
my $drive_old = "$ARGV[0]" or die "$usage\n";
my $drive_new = "$ARGV[1]" or die "$usage\n";

foreach ($drive_old, $drive_new) {
  $_ =~ s/(\/?dev\/?)([[:alnum:]]+)(\/?)/$2/;
}

#
sub GetDriveSize {
  my $kname = $_[0];
  open(my $dev_h, '<', "/dev/$kname") or die "open(/dev/$kname,ro) failed: $!";

  seek($dev_h, 0, 2);
  my $size = tell($dev_h);

  close($dev_h);
  return $size;
}
sub ReadSector {
  my $kname = $_[0];
  my $offset = $_[1];
  open(my $dev_h, '<', "/dev/$kname") or die "open(/dev/$kname,ro) failed: $!";

  # skip to the begin of the sector
  seek($dev_h, $offset, 0);

  # read sector into an array
  my @sector;
  for (my $i = 0; $i < 512; $i++) {
    $sector[$i] = getc($dev_h);
  }

  close($dev_h);
  return @sector;
}
sub ReadTable {
  my $kname = $_[0];
  my $offset = $_[1];

  my $partition_table = "";

  open(my $dev_h, '<', "/dev/$kname") or die "open(/dev/$kname,ro) failed: $!";

  seek($dev_h, $offset, 0);
  read( $dev_h, $partition_table, (512 * 32) );

  close($dev_h);
  return $partition_table;
}

# get drive sizes in bytes
my $drive_size_old = GetDriveSize("$drive_old");
my $drive_size_new = GetDriveSize("$drive_new");

# get old gpt header positions in bytes
my $pos_pri_header_old = 512;
my $pos_sec_header_old = $drive_size_old - 512;

# read old gpt headers into arrays
my @pri_header_old = ReadSector("$drive_old", $pos_pri_header_old);
my @sec_header_old = ReadSector("$drive_old", $pos_sec_header_old);

# read old protective mbr into an array
my @protective_mbr_old = ReadSector("$drive_old", 0);

# read old primary partition table into a scalar
my $primary_table_old = ReadTable("$drive_old", 1024);

#################################
#          SOME CHECKS          #
#################################

# check mbr signature
sub CheckMbrSignature {
  print("checking mbr boot signature...");
  #if ( join(@protective_mbr_old[510..511]) eq ( chr(0x55) . chr(0xaa) ) ) {
  if ( ( $protective_mbr_old[510] eq chr(0x55) ) and
       ( $protective_mbr_old[511] eq chr(0xaa) ) ) {
    print(" passed.\n");
  }
  else {
    print(" failed.\n");
  }
}
CheckMbrSignature();

# check partition type "protective mbr"
sub CheckPartTypeProtectiveMbr {
  print("checking partition type \"protective mbr\"...");
  if ( $protective_mbr_old[450] eq chr(0xee) ) {
    print(" passed.\n");
  }
  else {
    print(" failed.\n");
  }
}
CheckPartTypeProtectiveMbr();

# check gpt signatures on drive-old
sub CheckGptSignatures {
  my $gpt_signature = "EFI PART";

  my $gpt_signature_pri_header_old = pack('a' x 8, @pri_header_old[0..7]);
  my $gpt_signature_sec_header_old = pack('a' x 8, @sec_header_old[0..7]);

  sub OnFailure {
    print(" failed.\n");
    print(STDERR "--> cancelled.\n");
    exit 1;
  }

  print("checking gpt signature on primary gpt header...");
  if ($gpt_signature_pri_header_old eq $gpt_signature) {
    print( " passed.\n");
  }
  else {
    OnFailure();
  }

  print("checking gpt signature on secondary gpt header...");
  if ($gpt_signature_sec_header_old eq $gpt_signature) {
    print(" passed.\n");
  }
  else {
    OnFailure();
  }
}
CheckGptSignatures();

# check if drive-new is not smaller in size than drive-old
sub CheckDriveSizes {
  if ($drive_size_new < $drive_size_old) {
    print("\n/dev/$drive_new is smaller than /dev/$drive_old.\n");
    print("Still want to continue? [y/N] ");

    my $answer = <STDIN>;
    chomp($answer);   # removes the \n

    if ( $answer !~ m/^[yYjJ].*/ ) {
      print("\ncancelled...\n");
      exit(0);
    }
  }
}
CheckDriveSizes();

print("\n");

###############################
#          TRANSFORM          #
###############################

# get new gpt header positions in bytes
my $pos_pri_header_new = 512;
my $pos_sec_header_new = $drive_size_new - 512;

# transform pri-header-old to pri-header-new
my @pri_header_new = @pri_header_old;
sub Transform_PriHeaderNew {

  # offset 32 (+08): LBA location of sec-header-new
  @pri_header_new[32..39] = map(
    chr, unpack( "C8", pack( "Q<", ($pos_sec_header_new / 0x0200) ) )
  );

  # offset 48 (+08): location of last usable LBA (sec-part-table-first-LBA - 1)
  @pri_header_new[48..55] = map(
    chr, unpack( "C8", pack( "Q<", ($pos_sec_header_new / 0x0200) - 33 ) )
  );

  # offset 16 (+04): set to zero
  @pri_header_new[16..19] = map( chr, (0, 0, 0, 0) );

  # offset 16 (+04): crc32 from (00) to (91) in little endian
  my $crc32 = Archive::Zip::computeCRC32( join('', @pri_header_new[0..91]) );
  @pri_header_new[16..19] = map( chr, unpack( "C4", pack( "V", $crc32 ) ) );
}
Transform_PriHeaderNew();

# transform sec-header-old to sec-header-new
my @sec_header_new = @sec_header_old;
sub Transform_SecHeaderNew {

  # offset 24 (+08): location of sec-header-new
  splice(@sec_header_new, 24, 8, @pri_header_new[32..39]);

  # offset 32 (+08): location of pri-header-new
  splice(@sec_header_new, 32, 8, @pri_header_new[24..31]);

  # offset 48 (+08): location of last usable LBA (sec-part-table-first-LBA - 1)
  splice(@sec_header_new, 48, 8, @pri_header_new[48..55]);

  # offset 72 (+08): LBA of secondary table
  @sec_header_new[72..79] = map(
    chr, unpack( "C8", pack( "Q<", ($pos_sec_header_new / 0x0200) - 32 ) )
  );

  # offset 16 (+04): set to zero
  @sec_header_new[16..19] = map( chr, (0, 0, 0, 0) );

  # offset 16 (+04): crc32 from (00) to (91) in little endian
  my $crc32 = Archive::Zip::computeCRC32( join('', @sec_header_new[0..91]) );
  @sec_header_new[16..19] = map( chr, unpack( "C4", pack( "V", $crc32 ) ) );
}
Transform_SecHeaderNew();

# find position for secondary partition table on drive-new
my $pos_sec_table_new = $pos_sec_header_new - (512 * 32);

# transform protective-mbr-old to protective-mbr-new
my @protective_mbr_new = @protective_mbr_old;
my $protected_partition_sector_number = ($pos_sec_header_new / 512);
sub Transform_ProtectiveMbrNew {
  # offset 458 (+04): number of sectors in protected partition
  @protective_mbr_new[458..461] = map(
    chr, unpack( "C4", pack( "V", $protected_partition_sector_number ) )
  );
}
Transform_ProtectiveMbrNew();

###########################
#          PRINT          #
###########################

#
sub PrintHeader {
  my @header = @_;
  my $width = 16;
  my $sep_line = " ---- -----------------------+-----------------------";

  print("$sep_line\n");
  for (my $i = 0; $i < 92; $i++) {
    if ($i % $width == 0) {
      printf(" %04d", $i);
    }
    printf( " %02x", ord($header[$i]) );
    if ( ( ($i + 1) % $width == 0 ) or ($i == 91) ) {
      print("\n");
    }
  }
  print("$sep_line\n");
}
sub PrintOldHeaders {
  print("gpt headers on /dev/$drive_old are:\n");
  print("prim header, offset: $pos_pri_header_old\n");
  PrintHeader(@pri_header_old);
  print("sec header, offset : $pos_sec_header_old\n");
  PrintHeader(@sec_header_old);
  print("\n");
}
sub PrintNewHeaders {
  print("gpt headers on /dev/$drive_new will be:\n");
  print("prim header, offset: $pos_pri_header_new\n");
  PrintHeader(@pri_header_new);
  print("sec header, offset : $pos_sec_header_new\n");
  PrintHeader(@sec_header_new);
  print("\n");
}
sub PrintSector {
  my @sector = @_;
  my $width = 16;
  my $sep_line = " ---- -----------------------+-----------------------";

  print("$sep_line\n");
  for (my $i = 446; $i < 512; $i++) {
    if ( ($i + 2) % $width == 0 ) {
      printf(" %04d", $i);
    }
    printf( " %02x", ord($sector[$i]) );
    if ( ( ($i + 2 + 1) % $width == 0 ) or ($i == 511) ) {
      print("\n");
    }
  }
  print("$sep_line\n");
}
sub PrintOldProtectiveMbr {
  print("protective mbr table on /dev/$drive_old is:\n");
  PrintSector(@protective_mbr_old);
  print("\n");
}
sub PrintNewProtectiveMbr {
  print("protective mbr table on /dev/$drive_new will be:\n");
  PrintSector(@protective_mbr_new);
  print("\n");
}

# print gpt headers and protective mbrs to stdout
sub PrintAll {
  PrintOldHeaders();
  PrintNewHeaders();
  PrintOldProtectiveMbr();
  PrintNewProtectiveMbr();
}
if ($verbose) {
  PrintAll();
}

###########################
#          WRITE          #
###########################

# ask for block size when copying all data with dd
if (not $blksize and not $no_dd) {
  sub AskForBlockSize {
    print(
      "Set block size for dd or leave blank to confirm " .
      "the default (1M): "
    );

    $blksize = <STDIN>;
    chomp($blksize);

    if ("$blksize" =~ m/^\s?$/) {
      $blksize = "1M";
    }
    elsif ( "$blksize" !~ m/^(\d+([cwbK]|kB|[MGTPEZY]B?)|)$/ ) {
      print(STDERR "\'$blksize\' is no valid blocksize for dd. Aborting...\n");
      exit 1;
    }
  }
  AskForBlockSize();
}

# aks for final confirmation
print <<"EOF";
Caution! This will erase all data on /dev/$drive_new.
Type uppercase 'yes' to continue...
EOF
sub Confirm {
  my $confirm = <STDIN>;
  chomp($confirm);   # removes the \n
  if ("$confirm" ne "YES") {
    print("cancelled...\n");
    exit(0);
  }
}
Confirm();

# copy all data from drive-old to drive-new, use dd for performance reasons
if (not $no_dd) {
  print(
    "copying all data from /dev/$drive_old to /dev/$drive_new using dd...\n"
  );
  system(
    "dd if=/dev/$drive_old of=/dev/$drive_new bs=$blksize status=progress"
  );
}

# write pri-header-new to device-new
sub WriteSector {
  my $kname = $_[0];
  my $offset = $_[1];
  my $sector = $_[2];
  open(my $dev_h, '>', "/dev/$kname") or die "open(/dev/$kname,ro) failed: $!";

  # skip to the begin of the header
  seek($dev_h, $offset, 0);

  # write gpt header to the device
  print($dev_h $sector);

  close($dev_h);
}
print("writing new primary header to /dev/$drive_new...\n");
WriteSector( "$drive_new", $pos_pri_header_new, join('', @pri_header_new) );

# erase old sec-part-table and sec-header from device-new
sub EraseOldSecTable {

  # get position of old last usable LBA from old primary header offset 48 (+08)
  my $pos_sec_table_old = 0;
  for (my $i = 0; $i < 8; $i++) {
    $pos_sec_table_old += ord($pri_header_old[48 + $i]) * (0x100 ** $i);
  }
  $pos_sec_table_old += 1;    # the partition table starts one LBA later
  $pos_sec_table_old *= 512;  # offset in bytes
  #printf("%1d\n", $pos_sec_table_old);

  # create a 33 LBAs long block of zeros
  my $zeroblock_1_lba = "";
  for (my $i = 0; $i < 512; $i++) {
    $zeroblock_1_lba .= chr(0);
  }
  my $zeroblock_33_lbas = "";
  for (my $i = 0; $i < 33; $i++) {
    $zeroblock_33_lbas .= $zeroblock_1_lba;
  }

  # overwrite old secondary partition table and gpt header with zeros
  open(my $dev_h, '>', "/dev/$drive_new") or
    die "open(/dev/$drive_new,ro) failed: $!";
  seek($dev_h, $pos_sec_table_old, 0);
  print($dev_h $zeroblock_33_lbas);
  close($dev_h);
}
print("erasing old secondary gpt from /dev/$drive_new...\n");
EraseOldSecTable();

# write partition table(s) to drive-new
sub WriteTable {
  my $kname = $_[0];
  my $offset = $_[1];
  my $partition_table = $_[2];

  # write secondary partition table to device-new
  open(my $dev_h, '>', "/dev/$kname") or die "open(/dev/$kname,ro) failed: $!";
  seek($dev_h, $offset, 0);
  print($dev_h $partition_table);
  close($dev_h);
}
# write primary table if dd is not used
if ($no_dd) {
  print("writing new primary partition table to /dev/$drive_new...\n");
  WriteTable("$drive_new", 1024, $primary_table_old);
}
# write secondary table
print("writing new secondary partition table to /dev/$drive_new...\n");
WriteTable("$drive_new", $pos_sec_table_new, $primary_table_old);

# write sec-header-new to device-new
print("writing new secondary header to /dev/$drive_new...\n");
WriteSector( "$drive_new", $pos_sec_header_new, join('', @sec_header_new) );

# write protective-mbr-new to device-new
print("writing new protective mbr to /dev/$drive_new...\n");
WriteSector( "$drive_new", 0, join('', @protective_mbr_new) );










;
