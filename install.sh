#!/bin/bash

declare FILENAME
declare DIRNAME
declare PERM

f_install() {
  if [[ -e ${DIRNAME}${FILENAME} ]]; then

    declare ANS

    echo "${DIRNAME}${FILENAME} exists already."
    echo -n "overwrite? [y/N] "
    read ANS

    if ! [[ "${ANS}" =~ ^[yYjJ].* ]]; then
      exit 0
    fi

  fi

  install -o root -g root -m ${PERM} -p -D ${FILENAME} ${DIRNAME}
}

FILENAME="gptmigrate.pl"
DIRNAME="/usr/local/sbin/"
PERM="755"
f_install

FILENAME="gptmigrate.8.gz"
DIRNAME="/usr/local/man/man8/"
if ! [[ -e ${DIRNAME} ]]; then
  OLD_UMASK=$(umask)
  umask 022
  mkdir -p ${DIRNAME}
  umask ${OLD_UMASK}
fi
PERM="644"
f_install
